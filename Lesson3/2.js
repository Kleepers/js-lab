'use strict'

function createDebounceFunction (func, delay = 250) {
  if (typeof func !== 'function') {
    throw new TypeError(`${func} is not a function`)
  }
  if (typeof delay !== 'number' || isNaN(delay)) {
    throw new TypeError(`${delay} is not a valid number`)
  }
  let timeoutId;
  return function (...args) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => func(...args), delay);
  }
}

const log100 = (n) => console.log(n)

const debounceLog100 = createDebounceFunction(log100, 400)

setTimeout(() => debounceLog100(500), 500)

for (let i = 0; i < 10; i++) {
    debounceLog100(400)
}




