'use strict'

function myFilter (callback, thisArg = undefined) {
    if (typeof callback !== 'function') throw new Error (`${callback} is not a function`)
    const newArray = []
    for (let i = 0; i < this.length; i++) {
        if (callback.call(thisArg, this[i], i, this)) {
            newArray.push(this[i])
        }
    }
    return newArray
}

Array.prototype.myFilter = myFilter

let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
const range = {lower: 1, upper: 5};

console.log(array.myFilter(function (number) {
    return number >= this.lower && number <= this.upper
}, range))

console.log(array.myFilter(number => number % 2 === 0))




