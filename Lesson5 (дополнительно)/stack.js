class Stack {
    constructor(length = 10) {
        let isLengthValid = length > 1 || isNaN(length) || !Number.isInteger(length) || !isFinite(length)
        if (!isLengthValid) throw new Error('Stack length must be a positive integer');
        this.length = length;
        this.stack = [];
        this.top = -1;
    }

    static fromIterable(iterable) {
        if (!iterable) throw new Error('Iterable is empty');
        if (!(Symbol.iterator in Object(iterable))) throw new Error('Iterable is not iterable');
        let stack = new Stack();
        stack.length = iterable.length;
        for (let value of iterable) {
            stack.push(value);
        }
        return stack;
    }

    push(value) {
        if (this.top >= this.length - 1) throw new Error('Stack is full');
        this.top = this.top + 1;
        this.stack[this.top] = value;
    }
    pop() {
        if (this.isEmpty()) throw new Error('Stack is empty');
        let value = this.stack[this.top];
        this.top = this.top - 1;
        return value;
    }
    peek() {
        if (this.isEmpty()) return null;
        return this.stack[this.top];
    }
    toArray() {
        let array = [];
        let i = 0;
        while (!this.isEmpty()) {
            if (typeof this.stack[this.top] === 'object') {
                let copy = {};
                for (let key in this.stack[this.top]) {
                    copy[key] = this.stack[this.top][key];
                }
                array[i] = copy;
                i++
                this.pop();
            } else {
                array[i] = this.pop();
                i++
            }
        }
        for (let j=array.length; j>0; j--) {
            this.push(array[j-1]);
        }
        return array;
    }
    isEmpty() {
        return this.top === -1;
    }
}

const numberStack = Stack.fromIterable([1, 2, 3, 4, 5]);

console.log(numberStack.toArray());

console.log('Массив из стека:', numberStack.toArray());



