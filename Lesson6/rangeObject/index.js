const wrappers = document.querySelectorAll('.wrapper');

wrappers.forEach((wrapper) => {
    const observer = new ResizeObserver((entries) => {
        const range = document.createRange();
        const textContainerElement = wrapper.querySelector('.text-container');
        const textElement =  textContainerElement.querySelector('.text')
        textContainerElement.style.width = '';
        range.setStartBefore(textElement);
        range.setEndAfter(textElement);

        const clientRect = range.getBoundingClientRect();
        textContainerElement.style.width = `${clientRect.width}px`;
    });
    observer.observe(wrapper);
})
