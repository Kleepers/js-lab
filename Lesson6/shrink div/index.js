const textElements = document.querySelectorAll('.text');

    textElements.forEach((textElement) => {

        const observer = new ResizeObserver((entries) => {
            let calculatedWidth, width, height;
            textElement.style.width = '';
            width = textElement.clientWidth;
            height = textElement.offsetHeight;

            for (calculatedWidth = width; calculatedWidth; calculatedWidth--) {
                textElement.style.width = calculatedWidth + 'px';
                if (textElement.offsetHeight !== height) break;
            }

            textElement.style.width = (calculatedWidth + 1) + 'px';
        })
        observer.observe(textElement.parentElement);

    })




