let textContainerElements = document.querySelectorAll('.text-container');

function getLength(txt, element) {
    let span = document.createElement('span');
    if (txt === ' ') {
        span.innerHTML = '&nbsp;';
    } else {
        span.innerHTML = txt;
    }

    let elementContent = element.innerHTML;
    element.innerHTML = '';

    element.append(span);
    let length = span.offsetWidth;
    span.remove();

    element.innerHTML = elementContent;

    return length
}

textContainerElements.forEach((element) => {
    const observer = new ResizeObserver((entries) => {

        let words = element.textContent.split(' ');
        let lengthOfSpace = getLength(' ', element);
        let lengthOfLine = 0;
        element.style.width = '';
        let maxElementWidth = element.offsetWidth + 1;
        let maxLineLengthSoFar = 0;


        for (let i = 0; i < words.length; i++) {

            if (words[i] === '') continue;

            let currentWordLength = getLength(words[i], element);
            let acceptedLineLength;
            let proposedLineLength = lengthOfLine + (i === 0 ? 0 : lengthOfSpace) + currentWordLength;

            if (proposedLineLength > maxElementWidth) {
                acceptedLineLength = lengthOfLine;
                lengthOfLine = currentWordLength;
            } else {
                lengthOfLine = proposedLineLength;
                acceptedLineLength = lengthOfLine;
            }
            if (acceptedLineLength > maxLineLengthSoFar) {
                maxLineLengthSoFar = acceptedLineLength;
            }

        }

        if (maxLineLengthSoFar !== 0) {
            element.style.width = maxLineLengthSoFar + 1 + 'px';
        }
    })
    observer.observe(element.parentElement);
})

