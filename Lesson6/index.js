let rightIconElements = document.querySelectorAll('.right-icon');

rightIconElements.forEach(function(icon) {
    let text = icon.previousElementSibling;
    let parent = icon.parentElement;
    setIconPosition(icon, text)
    const observer = new ResizeObserver((entries) => {
        setIconPosition(icon, text)
    })
    observer.observe(parent);
})

function setIconPosition(icon, text) {
    icon.style.left = text.offsetWidth + 16 + 'px';
}


