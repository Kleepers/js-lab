"use strict"
function changeNumberingSystem() {
    const num = Number(prompt("Введите число"))

    const numberingSystem = Number(prompt('Введите систему счисления'))

    let isInputCorrect = ((num % 1 === 0) && (numberingSystem % 1 === 0) && (numberingSystem > 0))

    if (isInputCorrect) {
        console.log(Number(num.toString(numberingSystem)))
    } else {
        console.log('Некорректный ввод!')
        throw new Error('Некорректный ввод!')
    }
}


changeNumberingSystem()

