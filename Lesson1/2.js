"use strict"

function isValid(num) {

    if (!(num % 1 === 0) || isNaN(num)) {
        console.log('Некорректный ввод!')
        throw new Error('Некорректный ввод!')
    }

}

function sumDivide() {

    const num1 = Number(prompt('Введите первое число'))

    isValid(num1)

    const num2 = Number(prompt('Введите второе число'))

    isValid(num2)

    console.log(`Ответ: ${num1 + num2}, ${num1 / num2}`)
}

sumDivide()
