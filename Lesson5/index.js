import setKeyboard from "./keyboard.js";
class Calculator {
    constructor(previousOperandTextElement, currentOperandTextElement) {
        this.previousOperandTextElement = previousOperandTextElement;
        this.currentOperandTextElement = currentOperandTextElement;
        this.clear();
    }

    clear() {
        this.currentOperand = '';
        this.previousOperand = '';
        this.operation = undefined;
    }

    changeUnarySign() {
        this.currentOperand = -this.currentOperand;
        this.currentOperand = this.currentOperand.toString();
    }

    delete () {
        if (this.currentOperand === '' && this.operation !== undefined) {
            this.operation = undefined;
            this.currentOperand = this.previousOperand;
            this.previousOperand = '';
            return;
        }
        this.currentOperand = this.currentOperand.toString().slice(0, -1);
    }

    #rounder(number) {
        const roundedNumber = Math.round(number * 100000000) / 100000000;
        return roundedNumber.toString();
    }

    appendNumber(number) {
        let isNumberTooBig = this.currentOperand.toString() + number.toString() >= 2**47 - 1
            || this.currentOperand.toString() + number.toString() <= -(2**47);
        if (number === '.' && this.currentOperand.includes('.')) return;
        if (isNumberTooBig) return;
        if (this.currentOperand.split('.')[1] != null && this.currentOperand.split('.')[1].length >= 8) return;
        this.currentOperand = this.currentOperand.toString() + number.toString();
    }

    chooseOperation(operation) {
        if (this.currentOperand === '') return;
        if (Number(this.currentOperand) === 0 && this.operation === '/') return;
        if (this.previousOperand !== '') {
            this.calculate();
        }

        this.operation = operation;
        this.previousOperand = this.currentOperand;
        this.currentOperand = '';
    }

    calculate() {
        let computation;
        const prev = parseFloat(this.previousOperand);
        const current = parseFloat(this.currentOperand);
        if (isNaN(prev) || isNaN(current)) return;
        if (current === 0 && this.operation === '/') return;
        switch (this.operation) {
            case '+':
                computation = this.#rounder(prev + current);
                break;
            case '-':
                computation = this.#rounder(prev - current);
                break;
            case '*':
                computation = this.#rounder(prev * current);
                break;
            case '/':
                computation = this.#rounder(prev / current);
                break;
            default:
                return;
        }
        this.currentOperand = computation;
        this.operation = undefined;
        this.previousOperand = '';
    }

    #getDisplayNumber(number) {
        const stringNumber = number.toString();
        const integerDigits = parseFloat(stringNumber.split('.')[0]);
        const decimalDigits = stringNumber.split('.')[1];
        let integerDisplay;
        if (isNaN(integerDigits)) {
            integerDisplay = '';
        } else {
            integerDisplay = integerDigits.toLocaleString('en', { maximumFractionDigits: 0 });
        }
        if (decimalDigits != null) {
            return `${integerDisplay}.${decimalDigits}`;
        } else {
            return integerDisplay;
        }
    }

    updateDisplay() {
        this.currentOperandTextElement.innerText = this.#getDisplayNumber(this.currentOperand);
        if (this.operation != null) {
            this.previousOperandTextElement.innerText =
                `${this.#getDisplayNumber(this.previousOperand)}${this.operation}`;
        } else {
            this.previousOperandTextElement.innerText = '';
        }

    }
}

const calculatorElement = document.querySelector('.calculator');
const numberButtons = calculatorElement.querySelectorAll('[data-number]');
const operationButtons = calculatorElement.querySelectorAll('[data-operation]');
const equalsButton = calculatorElement.querySelector('[data-equals]');
const deleteButton = calculatorElement.querySelector('[data-delete]');
const clearButton = calculatorElement.querySelector('[data-clear]');
const unaryButton = calculatorElement.querySelector('[data-unary]');
const previousOperandTextElement = calculatorElement.querySelector('[data-previous-operand]');
const currentOperandTextElement = calculatorElement.querySelector('[data-current-operand]');

const calculator = new Calculator(previousOperandTextElement, currentOperandTextElement);

numberButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.appendNumber(button.innerText);
        calculator.updateDisplay();
    } )
})

operationButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.chooseOperation(button.innerText);
        calculator.updateDisplay();
    } )
})

equalsButton.addEventListener('click', () => {
    calculator.calculate();
    calculator.updateDisplay();
})

clearButton.addEventListener('click', () => {
    calculator.clear();
    calculator.updateDisplay();
})

deleteButton.addEventListener('click', () => {
    calculator.delete();
    calculator.updateDisplay();
})

unaryButton.addEventListener('click', () => {
    calculator.changeUnarySign();
    calculator.updateDisplay();
})

setKeyboard(calculator);









