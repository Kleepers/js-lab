export default function setKeyboard(calculator) {
    document.addEventListener('keydown', (event) => {
        console.log(event.key);
        const key = event.key;
        let isOperation = key === '+' || key === '-' || key === '*' || key === '/';
        if (key >= 0 && key <= 9 || key === '.') {
            calculator.appendNumber(key);
        }
        else if (isOperation) calculator.chooseOperation(key);
        else if (key === 'Enter' || key === '=') calculator.calculate();
        else if (key === 'Backspace') calculator.delete();
        else if (key === 'Delete') calculator.clear();
        calculator.updateDisplay();
    })
}

