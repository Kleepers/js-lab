'use strict'

class Calculator {
    constructor(x, y) {
        this.#checkNumberValidity(x)
        this.#checkNumberValidity(y)
        this.x = x
        this.y = y
    }
    #checkNumberValidity(num) {
        if (typeof num !== 'number' || isNaN(num) || !isFinite(num)) {
            throw new TypeError(`${num} is not a valid number`)
        }
    }
    setX(num) {
        this.#checkNumberValidity(num)
        this.x = num
    }
    setY(num) {
        this.#checkNumberValidity(num)
        this.y = num
    }
    logSum = () => {
        console.log(this.x + this.y)
    }
    logMul = () => {
        console.log(this.x * this.y)
    }
    logSub = () => {
        console.log(this.x - this.y)
    }
    logDiv = () => {
        if (this.y === 0) {
            throw new Error('Division by zero')
        }
        console.log(this.x / this.y)
    }
}

const calculator = new Calculator(12, 3);

calculator.logSum() // 15
calculator.logDiv() // 4
calculator.setX(15)
calculator.logDiv() // 5
const logCalculatorDiv = calculator.logDiv
logCalculatorDiv() // 5
// calculator.setY(444n) // Ошибка!


