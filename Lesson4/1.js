'use strict'

function concatStrings (string, separator) {

    let result = string

    let char = typeof separator === 'string' ? separator : ''

    if (typeof string !== 'string') { // если при первом вызове переменная string невалидная - возвращаю null
        return null
    }

    return function (stringArg, separatorArg) {
        let charArg = typeof separatorArg === 'string' ? separatorArg : char
        if (typeof stringArg === 'string') {
            return concatStrings(result + char + stringArg, charArg)
        } else {
            return result
        }
    }
}

console.log(concatStrings('Hello', ' ')('World', '!')('')()) // Hello World!

console.log(concatStrings('first', null)('second')()) // 'firstsecond'

console.log(concatStrings('first', '123')('second')('third')()) // 'first123second123third'

console.log(concatStrings('some-value')('')('')(null)) // 'some-value'

console.log(concatStrings('some-value')(2)) // 'some-value'

console.log(concatStrings('some-value')('333')(123n)) // 'some-value333' ?





