'use strict'

function selectFromInterval (array, start, end) {
    let intervalMin = 0
    let intervalMax = 0
    const isArrayValid =
                    Array.isArray(array) &&
                    array.length > 0 &&
                    array.every(number => typeof number === 'number' && !isNaN(number))

    if (Number.isInteger(start) && Number.isInteger(end)) {
        intervalMin = Math.min(start, end)
        intervalMax = Math.max(start, end)
    } else {
        throw new Error('Start and end must be integers')
    }

    if (isArrayValid) {
        return array.filter(item => item >= intervalMin && item <= intervalMax)
    } else {
        throw new Error('Invalid array')
    }
}

console.log(selectFromInterval([1, 3, 5], 5, 2))
console.log(selectFromInterval([-2, -15, 0, 4], -13, -5))
console.log(selectFromInterval(['aaa'], 2, 3))

