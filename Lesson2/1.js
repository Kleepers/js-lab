'use strict'

function makeObjectDeepCopy (obj) {
    let newObj = {}
    for (let key in obj) {
        if (typeof obj[key] === 'object') {
            newObj[key] = makeObjectDeepCopy(obj[key])
        } else {
            newObj[key] = obj[key]
        }
    }
    return newObj
}

let obj = {
    name: 'Vladislav',
    age: 21,
    address: {
        city: 'Volgograd',
        street: 'Batova',
        house: '4'
    }
}

let newObj = makeObjectDeepCopy(obj)

newObj.address.city = 'Moscow'
console.log(obj)
console.log(newObj)


